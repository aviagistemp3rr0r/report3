clear
clc
close all

%Load data
load iris.mat
X = iris(:,1:end-1);
true_labels = iris(:,end); 

%Training the SOM
x_length = 3;
y_length = 1;
gridsize=[y_length x_length];

% Test with different topologies, distance functions and epochs
topologies = {'hextop', 'gridtop', 'randtop'};
distanceFunctions = {'linkdist', 'boxdist', 'mandist'};
epochsSet = 0:1:200; epochsSet(1) = 1;
 
ARIs = zeros(length(topologies), length(epochsSet));

for j=1:length(distanceFunctions)
    for k=1:length(epochsSet)
        for i=1:length(topologies)
            net = newsom(X',gridsize,topologies{i},distanceFunctions{j});
            net.trainParam.epochs = epochsSet(k);
            net.trainParam.showWindow = 0;
            net = train(net,X');
            outputs = sim(net,X'); % Assigning examples to clusters
            [~,assignment]  =  max(outputs);            
            ARIs(i,k)=RandIndex(assignment,true_labels); % Compare clusters with true labels
        end
    end

    figure
    plot(ARIs')
    title(sprintf('ARI vs Epochs: SOM Iris (%s)', distanceFunctions{j}))
    legend(topologies, 'Location', 'southeast')
    %xticklabels(epochsSet)
    xlabel('Epoch * 10')    
    ylabel('Adjusted RandIndex')
end