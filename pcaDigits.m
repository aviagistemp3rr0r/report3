% TODO: PCA on handwritten digit 3

clear all; close all; clc;

%% Digit 3
load threes.mat -ascii
p = threes;

% Show i-th image of a digit 3
i = 5;
colormap('gray')
imagesc(reshape(threes(i, :), 16, 16), [0,1])

% TODO: Compute the mean 3 and display it
mean3 = mean(p);
colormap('gray')
imagesc(reshape(mean3, 16, 16), [0,1])

pCovarianceMatrix = cov(p); % TODO: Covariance matrix of all 3s
[eigVectors, eigValues] = eigs(pCovarianceMatrix, 3); % TODO: Compute eigVal & eigVectors of cov
plot(diag(eigValues)) % TODO: Plot the diagonal of the eigen values
title('Diagonal of 3 eigen values: PCA reconstruction (digit 3)')
xlabel('k')
ylabel('Eigen Value')


%% Perform Process PCA project & reconstruction

% Reconstruction MSE with 1, 2, 3, 4 and 256 components (k)
mseK1 = pcaProjectConstruct(p, 1)
mseK2 = pcaProjectConstruct(p, 2)
mseK3 = pcaProjectConstruct(p, 3)
mseK4 = pcaProjectConstruct(p, 4)
mseK256 = pcaProjectConstruct(p, 256)

maxPcaComponents = 50; % Count of PCA components
digit3PcaReconstructionMse = zeros(1, maxPcaComponents);

for k = 1:maxPcaComponents
    digit3PcaReconstructionMse(1, k) = pcaProjectConstruct(p, k); % Custom PCA
end

figure
plot(digit3PcaReconstructionMse)
title('MSE vs PCA components: PCA reconstruction (digit 3)')
xlabel('PCA components')
ylabel('MSE')

% TODO: cumsum for 256 components
maxPcaComponents = 256; % Count of PCA components
digit3PcaReconstructionMseDiag = zeros(2, maxPcaComponents);
for k = 1:maxPcaComponents
    [digit3PcaReconstructionMseDiag(2, k), ~, ~, eigValues] = pcaProjectConstruct(p, k); % Custom PCA
    cumSumEigValues = cumsum(eigValues);
    digit3PcaReconstructionMseDiag(1, k) = cumSumEigValues(end);
end

figure
plot(digit3PcaReconstructionMseDiag(1,1:50), digit3PcaReconstructionMseDiag(2,1:50))
title('MSE vs Eigen Value cumulative sum: PCA reconstruction (digit 3, k = 1:50)')
xlabel('Eig Value cumulative sum')
ylabel('MSE')

%% PCA projection and reconstruction function
function [mseError, xProjected, xhat, eigenValuesDiag] = pcaProjectConstruct(x, k)
    [v, eigenValues] = eigs(cov(x), k);
    eigenValuesDiag = diag(eigenValues);
    xProjected = x * v;
    xhat = xProjected * v';
    mseError = sqrt(mean(mean(x - xhat).^2));
end