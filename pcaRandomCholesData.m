% PCA using custom method & process PCA on Random & correlated data

clear all; close all; clc;

%% Redundancy & Random Data

% Random Data
x = randn(50, 500); % Input data
pcaComponents = 5;
xPcaReconstructionMse = pcaProjectConstruct(x, pcaComponents)

% Correlated Data with custom PCA
load choles_all.mat
cholessCustomPcaMse = pcaProjectConstruct(p, pcaComponents)

% Correlated Data with Process PCA
mapstd(p);
maxfrac = 0.001;
[z, settings] = processpca(p, 'maxfrac', maxfrac);
phat = processpca('reverse', z, settings);
cholesProcessPcaMse = sqrt(mean(mean(p - phat).^2))

%% PCA projection and reconstruction
function [mseError, xProjected, xhat] = pcaProjectConstruct(x, k)
    [v, d] = eigs(cov(x), k);
    xProjected = x * v;
    xhat = xProjected * v';
    mseError = sqrt(mean(mean(x - xhat).^2));
end